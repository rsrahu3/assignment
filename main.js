var globalData = [];
var initialCanvas = '';

function getData(){
	fetch("https://jsonplaceholder.typicode.com/photos").then(function(response){
		response.json().then(function(data){
			globalData.push(data[0]);
			globalData.push(data[1]);
			globalData.push(data[data.length-1]);
			globalData.push(data[data.length-2]);
			globalData.push(data[34]);
			
		});
	}).catch(function(err){
		alert("Something went wrong.Try again by refresing");
	});

}

function generateCanvas(){
	
	var number = Math.floor(Math.random()*10);
	number = number>4?4:(number<2?2:number);
	var parentDiv = document.getElementById("canvasDiv");
	parentDiv.innerHTML = "";
	var x = document.getElementById("selectBox");
	x.options.length = 0;
	var option = document.createElement("option");
	option.text = "Canvas #";
	x.add(option);
	for(var i = 0 ;i < number;i++){
		//adding select options
		var option = document.createElement("option");
		option.text = i+1;
		x.add(option);
		//adding inner div,it will contain canvas
		var div = document.createElement("div");
		div.setAttribute("class" , "canvasParentDiv");
		div.id = "innerDiv" + (i*1+1);
		//canvas creation
		var canvas = document.createElement("canvas");
		canvas.setAttribute("class" , "canvas");
		canvas.id = "canvas"+(i*1+1);
		div.appendChild(canvas);
		parentDiv.appendChild(div);
		
		var fabCanvas = new fabric.Canvas(canvas.id);
		fabCanvas.setHeight(400);
		fabCanvas.setWidth(400);
		//event attachment
		fabCanvas.on('mouse:down', function() {
			if(this.getActiveObject()) {
				activeObject  = $.extend({}, this.getActiveObject());
				initialCanvas = this.lowerCanvasEl.id;
			}
		});
		
		$("#"+div.id).on('mouseup', function(evt) {
			var target = evt.currentTarget;
			var left  = evt.pageX -  target.offsetLeft;
			var top = evt.pageY - target.offsetTop;
			if(evt.target.localName === 'canvas' && initialCanvas) {
				canvasId = $(evt.target).siblings().attr('id');
				if(canvasId !== initialCanvas) {
					var prevCanvas = document.getElementById(initialCanvas).fabric;
					prevCanvas.remove(prevCanvas.getActiveObject());
					activeObject.left = left;
					activeObject.top = top;
					var targetCanvas = document.getElementById(canvasId).fabric;
					targetCanvas.add(activeObject);
					targetCanvas.renderAll();
				}
			}
			initialCanvas = '';
			activeObject  = {};                 
		});    
		canvas.fabric = fabCanvas;
	}
}

function insertComponent(){
	hightLightCanvas(true);
	var e = document.getElementById("selectBox");
    var val = e.options[e.selectedIndex].value;
	if(val == "null"){
		alert("Please Select Canvas Number");
		return;
	}
	var canvas = document.getElementById('canvas'+val).fabric;
 
	var left = 0,top = 0,width = 100 , height = 80;
	for(var i=0;i<globalData.length;i++){
		if(globalData[i].albumId>=100){
			var textBox = getTextBox(globalData[i].url,width,left,top);
			canvas.add(textBox); 
			top = top + height;
		}
		else if(globalData[i].id % 2 != 0 ){
			fabric.Image.fromURL(globalData[i].thumbnailUrl, function(myImg) {
				var img1 = myImg.set({ left: left, top: top ,width:width,height:height});
				canvas.add(img1); 
				left = left+width+20;
			});
		}
		else{
				var textBox = getTextBox(globalData[i].title,width,left,top);
				canvas.add(textBox); 
				top = top + height;
		}
		
	}
}
function hightLightCanvas(flag){
	var select = document.getElementById("selectBox");
	var numberOfOption = select.options.length;
	for(var i=0;i< numberOfOption;i++){
		var div = document.getElementById("innerDiv"+(i+1));
		if(div){
			if(flag){
				document.getElementById("canvas"+(i+1)).fabric.clear();
			}
			else{
				document.getElementById("innerDiv"+(i+1)).style.border = "5px solid black";
			}
		}
	}
	document.getElementById("innerDiv"+select.value).style.border = "5px solid red";
}

function getTextBox(text,width,left,top){
	
	var t1 = new fabric.Textbox(text, {
				width: width,
				top: top,
				left: left,
				fontSize: 16,
				textAlign: 'center',
				fixedWidth: 150
			});
	return t1;
}
getData();
